<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: wuwu <15093565100@163.com>
// +----------------------------------------------------------------------
namespace api\portal\controller;

use api\portal\service\PortalPostService;
use app\admin\model\LinkModel;
use cmf\controller\RestBaseController;

class SiteController extends RestBaseController
{
    /**
     * 网站设置
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {

        $type = $this->request->param('type', 'site_info');

        if (empty($type)) {
            $this->error('类型不能空！');
        }

        $info = cmf_get_option($type);
        if(!empty($info['site_contact'])){
            $info['site_contact'] = explode(',',$info['site_contact']);
        }else{
            $info['site_contact'] = '';
        }
        if (empty($info)) {
            $this->error('没有数据');
        } else {
            $this->success('ok', $info);
        }
    }

    public function getLinks(){

        $linkModel = new LinkModel();
        $links     = $linkModel->order('list_order', 'desc')->select();
        if (empty($links)) {
            $this->error('没有数据');
        } else {
            $this->success('ok', $links);
        }
    }

}
