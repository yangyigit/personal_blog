<?php
/**
 * Created by PhpStorm.
 * User: yangyi
 * Date: 2020/3/28 0028
 * Time: 14:41.
 */

namespace api\portal\controller;

use cmf\controller\RestBaseController;
use api\portal\service\PortalPostService;
use think\Db;

class RecommendController extends RestBaseController
{
    public function index()
    {
        $params      = $this->request->get();
        $postService = new PortalPostService();
        $data        = $postService->postArticles(['recommended'=>1],$params);
        //按分类获取全部文章列表
        $art_post = Db::name('portal_category_post')
            ->field('pcp.post_id,category_id,name,description')
            ->alias('pcp')
            ->leftJoin('portal_category pc', 'pc.id = pcp.category_id')
            ->where('pcp.status', 1)
            ->order('pcp.list_order desc')
            ->select()
            ->toArray();
        foreach ($art_post as $apk => $apv) {
            foreach ($data as $kdata => $vdata) {
                $data[$kdata]['tag'] = explode(',', $vdata['post_keywords']);
                if ($vdata['id'] == $apv['post_id']) {
                    $art_post[$apk]['child'][] = $vdata;
                }
            }
            if(empty($art_post[$apk]['child'])){
                unset($art_post[$apk]);
            }

        }
        $all_data = array();
        foreach ($art_post as $k=>$v) {
            if (!isset($all_data[$v['category_id']])) {
                $all_data[$v['category_id']] = $v;
            } else {
                $all_data[$v['category_id']]['child'][] = $v['child'][0];
            }
        }
        $this->success('请求成功!', $all_data);
    }
}
