<?php
/**
 * Created by PhpStorm.
 * User: yangyi
 * Date: 2020/4/3 0003
 * Time: 20:50
 */

namespace api\portal\controller;

use api\user\model\CommentModel;
use cmf\controller\RestBaseController;

class CommentController extends RestBaseController
{

    public function getList(){

        $model = new CommentModel();

        $objectId = $this->request->param('object_id');

        $data = $model->where(['object_id'=>$objectId,'status'=>1])->field('id,parent_id,object_id,content,full_name,to_user_name,create_time')->select()->toArray();

        if($data){
            $list = make_tree($data,$pk='id',$pid='parent_id');
        }else{
            $list = [];
        }
        $this->success('ok', ['list' => $list]);
    }

    public function addComment(){

        $verify = [
            "object_id",
            "full_name",
            "email",
            "content",
            "more",
        ];
        $data = $this->checkField($verify);

        $commentModel = new CommentModel();
        $postData = [
            'object_id' => $data['object_id'],
            'full_name' => $data['full_name'],
            'email' => $data['email'],
            'content' => $data['content'],
            'more' => $data['more'],
            'status' => 0,
            'table_name' => 'portal_post',
            'create_time' => time(),
        ];

        $res = $commentModel->save($postData);

        if($res){
            $this->success('ok');
        }else{
            $this->error('error');
        }
    }
}