<?php
/**
 * Created by PhpStorm.
 * User: yangyi
 * Date: 2020/3/27 0027
 * Time: 21:50
 */

/**
 * @param $items
 * @return array
 * 把数组整理成树状结构
 */
/*$items = array(
    1 => array('id' => 1, 'pid' => 0, 'name' => 'FP成品', 'value' => 'FP', 'checked' => false),
    2 => array('id' => 2, 'pid' => 1, 'name' => 'FP01成品串口服务器', 'value' => 'FP01', 'checked' => false),
    3 => array('id' => 3, 'pid' => 2, 'name' => 'FP0100成品串口服务器', 'value' => 'FP0100', 'checked' => false),
    4 => array('id' => 4, 'pid' => 3, 'name' => 'FP010002成品串口服务器ATMEL-E18', 'value' => 'FP010002', 'checked' => false),
    5 => array('id' => 5, 'pid' => 3, 'name' => 'FP010003成品串口服务器TI TM4C129E', 'value' => 'FP010003', 'checked' => false),
    6 => array('id' => 6, 'pid' => 1, 'name' => 'FP02成品网关', 'value' => 'FP02', 'checked' => false),
);*/
function make_tree($list,$pk='id',$pid='pid',$child='children',$root=0){
    $tree=array();
    foreach($list as $key=> $val){
        if($val[$pid]==$root){
            //获取当前$pid所有子类
            unset($list[$key]);
            if(! empty($list)){
                $child=make_tree($list,$pk,$pid,$child,$val[$pk]);
                if(!empty($child)){
                    $val['children']=$child;
                }
            }
            $tree[]=$val;
        }
    }
    return $tree;
}